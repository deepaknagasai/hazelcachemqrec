package com.xstlToxml;

import java.io.StringWriter;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;

import ParseXmltoJson.XmltoJson;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ TransformTest.class, XmltoJson.class,TransformerFactory.class,StringWriter.class })

public class TestTransformTest {

	@Mock
	TransformerException te;
	
	@Mock
	TransformerConfigurationException tce;
	
	@Mock
	TransformTest tt;
	@Mock
	TransformerFactory factory;
	
	
		String in = "</catalog><cd><id>1</id><title>Empire Burlesque</title><artist>Sowjanya</artist><country>USA</country><company>Columbia</company><price>10.90</price><year>1985</year></cd></catalog>";
		String inXSL = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
				+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
				+"<xsl:output method=\"xml\" version=\"1.0\" encoding=\"UTF-8\" indent=\"yes\"/>"
				+"<xsl:template match=\"/\">"
				+"<xsl:element name=\"root\">"
				+"<xsl:apply-templates select=\"catalog/cd/id\"/>"
				+"<xsl:apply-templates select=\"catalog/cd/artist\"/>"
				+"<xsl:apply-templates select=\"catalog/cd/country\"/>"
			    +"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/id\">"
				+"<xsl:element name=\"id\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/artist\">"
				+"<xsl:element name=\"singer\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/country\">"
				+"<xsl:element name=\"place\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"				
				+"</xsl:stylesheet>";
		/*
		String out = "";*/
		String out = null;
	@Test
		public void testTransformTestExceptionCase(){
		try{
			tt = PowerMockito.mock(TransformTest.class);
			PowerMockito.whenNew(TransformTest.class).withNoArguments().thenReturn(tt);
			//PowerMockito.mockStatic(XSLTTranformation.class);
			PowerMockito.when(new TransformTest(), "transform", in, inXSL, out).thenThrow(new TransformerConfigurationException());
			TransformTest.demo(in);
			System.out.println("test1 tce executed");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			tt = PowerMockito.mock(TransformTest.class);
			PowerMockito.whenNew(TransformTest.class).withNoArguments().thenReturn(tt);
			//PowerMockito.mockStatic(XSLTTranformation.class);
			PowerMockito.when(new TransformTest(), "transform", in,inXSL, out).thenThrow(te);
			TransformTest.demo(in);
			System.out.println("test1 te executed");
		}catch(Exception e){
			e.printStackTrace();
		}
	
	
		
	}
	@SuppressWarnings("static-access")
	@Test
	public void testTransformTestNoException() {
		String in="<catalog><cd><id>1</id><title>Empire Burlesque</title><artist>Bob Dylan</artist><country>USA</country><company>Columbia</company><price>10.90</price><year>1985</year></cd></catalog>";
	
		try {
			
			String str = PowerMockito.mock(String.class);
			XmltoJson sample = PowerMockito.mock(XmltoJson.class);
			PowerMockito.whenNew(XmltoJson.class).withNoArguments().thenReturn(sample);
			PowerMockito.when(sample.toJSON(Mockito.anyString())).thenReturn(str);
					TransformTest.demo(in);
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
