package com.HazelCast;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HazelCastCheck.class, HazelcastInstance.class, ClientConfig.class, GroupConfig.class,
	ClientNetworkConfig.class, HazelcastClient.class, IMap.class, Collection.class })
public class Sample {


	@SuppressWarnings({ "unchecked" })
	@Test
	public void test()
	{
		
		ClientConfig cc = PowerMockito.mock(ClientConfig.class);
		ClientNetworkConfig cnc = PowerMockito.mock(ClientNetworkConfig.class);
		GroupConfig groupConfig = PowerMockito.mock(GroupConfig.class);
		HazelcastInstance hz = PowerMockito.mock(HazelcastInstance.class);
		//HazelcastClient hc = PowerMockito.mock(HazelcastClient.class);
		HazelCastCheck hcc = new HazelCastCheck();
		IMap imap = PowerMockito.mock(IMap.class);
		String str1 = PowerMockito.mock(String.class);
		String  num="1";

		try {
			PowerMockito.whenNew(ClientConfig.class).withNoArguments().thenReturn(cc);
			PowerMockito.when(cc.getGroupConfig()).thenReturn(groupConfig);
			PowerMockito.when(groupConfig.setName("dev")).thenReturn(groupConfig);
			PowerMockito.when(groupConfig.setPassword("dev-pass")).thenReturn(groupConfig);
			PowerMockito.when(cc.getNetworkConfig()).thenReturn(cnc);
			PowerMockito.when(cnc.addAddress(Mockito.anyString())).thenReturn(cnc);
			
			PowerMockito.mockStatic(HazelcastClient.class);
			PowerMockito.when(HazelcastClient.newHazelcastClient(cc)).thenReturn(hz);
			PowerMockito.when(hz.getMap(Mockito.anyString())).thenReturn(imap);
			PowerMockito.when(imap.put(Mockito.anyObject(), Mockito.anyObject())).thenReturn(str1);
			PowerMockito.when(imap.containsKey(num)).thenReturn(true);
			hcc.check("6");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
