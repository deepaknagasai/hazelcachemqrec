package com.HazelCast;



	import java.net.UnknownHostException;
	import java.util.ArrayList;
	import java.util.Collection;

	import org.junit.Test;
	import org.junit.runner.RunWith;
	import org.mockito.InjectMocks;
	import org.mockito.Mock;
	import org.mockito.Mockito;
	import org.powermock.api.mockito.PowerMockito;
	import org.powermock.core.classloader.annotations.PrepareForTest;
	import org.powermock.modules.junit4.PowerMockRunner;
	import com.hazelcast.client.HazelcastClient;
	import com.hazelcast.client.config.ClientConfig;
	import com.hazelcast.client.config.ClientNetworkConfig;
	import com.hazelcast.config.GroupConfig;
	import com.hazelcast.core.HazelcastInstance;
	import com.hazelcast.core.IMap;

	@RunWith(PowerMockRunner.class)
	@PrepareForTest({ HazelCastCheck.class, HazelcastInstance.class, ClientConfig.class, GroupConfig.class,
		ClientNetworkConfig.class, HazelcastClient.class, IMap.class, Collection.class })
	public class HazelCastChecktest2 {
		@Mock
		IMap<Object, Object> imap;

		@Mock
		HazelcastInstance connection;

		@Mock
		HazelcastInstance hazelcastInstance;

		@InjectMocks
		HazelCastCheck hazelCastCheckValues;

		@Mock
		ClientConfig clientConfig;

		@Mock
		GroupConfig groupConfig;

		@Mock
		HazelcastClient hazelcastClient;

		@Mock
		ClientNetworkConfig clientNetworkConfig;
		// Test Case 2

				@SuppressWarnings("unchecked")
				@Test
				public void testCheckElseBlock() throws Exception {
					//Collection<Object> list = new ArrayList<Object>();
					String num="1";
					try {
						hazelcastClient = PowerMockito.mock(HazelcastClient.class);
						clientConfig = PowerMockito.mock(ClientConfig.class);
						groupConfig = PowerMockito.mock(GroupConfig.class);
						clientNetworkConfig = PowerMockito.mock(ClientNetworkConfig.class);
						imap = PowerMockito.mock(IMap.class);

						PowerMockito.whenNew(ClientConfig.class).withNoArguments().thenReturn(clientConfig);
						PowerMockito.whenNew(ClientNetworkConfig.class).withNoArguments().thenReturn(clientNetworkConfig);

						PowerMockito.when(clientConfig.getGroupConfig()).thenReturn(groupConfig);
						PowerMockito.when(clientConfig.getNetworkConfig()).thenReturn(clientNetworkConfig);

						PowerMockito.when(groupConfig.setName(Mockito.anyString())).thenReturn(groupConfig);
						PowerMockito.when(groupConfig.setPassword(Mockito.anyString())).thenReturn(groupConfig);

						hazelcastInstance = PowerMockito.mock(HazelcastInstance.class);
						PowerMockito.mockStatic(HazelcastClient.class);
						PowerMockito.when(HazelcastClient.newHazelcastClient(clientConfig)).thenReturn(hazelcastInstance);

						PowerMockito.when(hazelcastInstance.getMap(Mockito.anyString())).thenReturn(imap);

						//PowerMockito.when(imap.values()).thenReturn(list);
						PowerMockito.when(imap.containsKey(num)).thenReturn(true);


						HazelCastCheck.check("6");

					} catch (Exception e) {
						e.printStackTrace();
						assert (true);
					}
				}


		
}
