package ParseXmltoJson;

import javax.xml.transform.TransformerConfigurationException;

import org.json.JSONObject;
import org.json.XML;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.HazelCast.HazelCastCheck;
import com.JsonTomq.JsonToPostMq;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.xstlToxml.TransformTest;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ XmltoJson.class,JsonToPostMq.class,XML.class,JSONObject.class,HazelCastCheck.class })


public class XmltoJsonTest {
	
	//String jsonPrettyString;

	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void TestToJSON() throws Exception{
		//PowerMockito.mockStatic(XML.class);
//		XML xml=PowerMockito.mock(XML.class);
		//String str = "{\"balance\": 1000.21, \"num\":100, \"is_vip\":true, \"name\":\"foo\"}";
//		JSONObject json=new JSONObject("{\"balance\": 1000.21, \"num\":100, \"is_vip\":true, \"name\":\"foo\"}") ;
		//json= "{"balance": 1000.21, "num":100, "is_vip":true, "name":"foo"}";
//        json = PowerMockito.mock(JSONObject.class);
       // PowerMockito.whenNew(JSONObject.class).withNoArguments().thenReturn(json);
//		PowerMockito.when(xml.toJSONObject(Mockito.anyString())).thenReturn(json);
			
		/*try{
		PowerMockito.mockStatic(JsonToPostMq.class);

		PowerMockito.doNothing().when(JsonToPostMq.class, "JsonToPostMethod", "dummy1", "dummy2");
		XmltoJson xmltoJson =new XmltoJson();
		xmltoJson.toJSON("inputXML");
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		// JsonToPostMq.JsonToPostMethod(id,msg);*/
		XmltoJson xmlTo=new XmltoJson();
		String str1 = PowerMockito.mock(String.class);
		PowerMockito.mockStatic(JsonToPostMq.class);
		PowerMockito.doNothing().when(JsonToPostMq.class, "JsonToPostMethod", Mockito.anyString(), Mockito.anyString());
		//PowerMockito.when(new JsonToPostMq(), "JsonToPostMethod", "1", "hello").thenThrow(Exception.class);
		
		xmlTo.toJSON("<root>"+"<singer>Bob Dylan</singer>"+"<id>1</id>"+"</root>");
		
		//JsonToPostMq.JsonToPostMethod("1","hello");
		
		
		}

}
