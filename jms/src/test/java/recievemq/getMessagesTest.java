package recievemq;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.transform.TransformerConfigurationException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import javax.jms.Message;
import com.JsonTomq.JsonToPostMq;
import com.xstlToxml.TransformTest;
import ParseXmltoJson.XmltoJson;



@RunWith(PowerMockRunner.class)
@PrepareForTest({getMessages.class, ConnectionFactory.class, ActiveMQConnectionFactory.class,TransformTest.class})
public class getMessagesTest{
	getMessages gm = new getMessages();	

	//static String str="hello";
@SuppressWarnings("static-access")
	@Test
	
	public void testSendMessage() throws Exception {
		String test = "Hello";
		getMessages gm = new getMessages();	
		Connection cmock = PowerMockito.mock(Connection.class);
		Session smock = PowerMockito.mock(Session.class);
		Queue qmock = PowerMockito.mock(Queue.class);
		TextMessage Tmock = PowerMockito.mock(TextMessage.class);
		String str=PowerMockito.mock(String.class);
		Message mmock = PowerMockito.mock(Message.class);
		MessageConsumer mcmock = PowerMockito.mock(MessageConsumer.class);
		ActiveMQConnectionFactory amconmock = PowerMockito.mock(ActiveMQConnectionFactory.class);
		PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(amconmock);
		//PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://172.17.16.159:61616" ).thenReturn(amconmock);
		PowerMockito.when(amconmock.createConnection()).thenReturn(cmock);
		PowerMockito.when(cmock.createSession(any(Boolean.class), any(Integer.class))).thenReturn(smock);
		PowerMockito.when(smock.createQueue(any(String.class))).thenReturn(qmock);
		PowerMockito.when(smock.createConsumer(qmock)).thenReturn(mcmock);
		PowerMockito.when(mcmock.receive()).thenReturn(Tmock);
		//PowerMockito.when(Tmock.getText()).thenReturn(str);
		PowerMockito.when(Tmock.getText()).thenThrow(new JMSException("stop"));
		PowerMockito.mockStatic(TransformTest.class);
		//PowerMockito.doNothing().when(TransformTest.class, "demo", str);
		PowerMockito.whenNew(TransformTest.class).withNoArguments().thenThrow(new JMSException("dummy"));
		gm.get();
		assertTrue(true);
		
	}
	
@SuppressWarnings("static-access")
	@Test
	public void ExceptionSendMessage() throws Exception {

		
		ActiveMQConnectionFactory amconmock = PowerMockito.mock(ActiveMQConnectionFactory.class);
		PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(amconmock);
		//PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://172.17.16.159:61616" ).thenReturn(amconmock);
		
		PowerMockito.when(amconmock.createConnection()).thenThrow(new JMSException("dummy"));
		gm.get();
		assertTrue(true);
	}

	
	

}