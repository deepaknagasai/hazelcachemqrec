package jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;



@SpringBootApplication
public class RecieverApp extends SpringBootServletInitializer {
	 // Used when run as jar
	
	 public static void main(String[] args) {
	        // Launch the application
		  SpringApplication.run(RecieverApp.class, args);

}
	 // Used when run as WAR
     
	    @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	        return builder.sources(RecieverApp.class);
	    }
}
