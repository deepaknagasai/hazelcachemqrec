package com.xstlToxml;



import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import ParseXmltoJson.XmltoJson;
public class TransformTest {
	
	static String inputXML = null;
	static String outputXML = null;
	static String inputXSL  = /*"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
			+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
			+"<xsl:output method=\"xml\" version=\"1.0\" encoding=\"UTF-8\" indent=\"yes\"/>"
			+"<xsl:template match=\"/\">"
			+"<xsl:element name=\"root\">"
			+"<xsl:apply-templates select=\"catalog/cd/id\"/>"
			+"<xsl:apply-templates select=\"catalog/cd/artist\"/>"
			+"<xsl:apply-templates select=\"catalog/cd/country\"/>"
		    +"</xsl:element>"
			+"</xsl:template>"
			+"<xsl:template match=\"catalog/cd/id\">"
			+"<xsl:element name=\"unique_id\">"
			+"<xsl:value-of select=\".\"/>"
			+"</xsl:element>"
			+"</xsl:template>"
			+"<xsl:template match=\"catalog/cd/artist\">"
			+"<xsl:element name=\"singer\">"
			+"<xsl:value-of select=\".\"/>"
			+"</xsl:element>"
			+"</xsl:template>"
			+"<xsl:template match=\"catalog/cd/country\">"
			+"<xsl:element name=\"place\">"
			+"<xsl:value-of select=\".\"/>"
			+"</xsl:element>"
			+"</xsl:template>"
			
			+"</xsl:stylesheet>";
	*/
			 "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
				+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
				+"<xsl:output method=\"xml\" version=\"1.0\" encoding=\"UTF-8\" indent=\"yes\"/>"
				+"<xsl:template match=\"/\">"
				+"<xsl:element name=\"root\">"
				+"<xsl:apply-templates select=\"catalog/cd/id\"/>"
				+"<xsl:apply-templates select=\"catalog/cd/artist\"/>"
				+"<xsl:apply-templates select=\"catalog/cd/country\"/>"
			    +"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/id\">"
				+"<xsl:element name=\"id\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/artist\">"
				+"<xsl:element name=\"singer\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"
				+"<xsl:template match=\"catalog/cd/country\">"
				+"<xsl:element name=\"place\">"
				+"<xsl:value-of select=\".\"/>"
				+"</xsl:element>"
				+"</xsl:template>"
				
				+"</xsl:stylesheet>";
	/*static String inputXSL= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                           +"<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
                           +"<xsl:output method=\"xml\" version=\"1.0\" encoding=\"UTF-8\" indent=\"yes\"/>"
                            +"<xsl:template match=\"/\">"
                             +"<xsl:for-each select=\"catalog/cd\">"
                              +"<xsl:value-of select=\"title\"/>"
                              +" <xsl:value-of select=\"artist\"/>" 
                               +"</xsl:for-each>"
                               +"</xsl:template>"
                               +"</xsl:stylesheet>";
			*/
				    /**
	     * @param args the command line arguments
				     * @throws TransformerException 
	     */
	    public static void demo(String inputXML) throws TransformerException
	    {
	        
	    	TransformTest st = new TransformTest();
	        try
	        {
	            st.transform(inputXML, inputXSL, outputXML);
	        }
	        catch (TransformerConfigurationException e)
	        {
	            System.err.println("TransformerConfigurationException");
	            System.err.println(e);
	        }
	        /*catch (TransformerException e)
	        {
	            System.err.println("TransformerException");
	            System.err.println(e);
	        }*/
	    }

	    public void transform(String inputXML, String inputXSL, String outputXML)
	            throws TransformerConfigurationException,
	            TransformerException
	    {

	        TransformerFactory factory = TransformerFactory.newInstance();
	        StreamSource xslStream = new StreamSource(new StringReader(inputXSL));
	        Transformer transformer = factory.newTransformer(xslStream);
	        StreamSource in = new StreamSource(new StringReader(inputXML));	        
	        StringWriter outputWriteXML = new StringWriter();
			StreamResult out = new StreamResult(outputWriteXML);
	        transformer.transform(in, out);
	        System.out.println("The generated XML file is:" + outputWriteXML);
	        XmltoJson xmltoJson = new XmltoJson();
	        xmltoJson.toJSON(outputWriteXML.toString());
	       // System.out.println(outputWriteXML.toString());
	    }
	}
	
	
/*static String outputXML=null;
public static void xsltTranform(String inputXML)
 {
     	        
				String inputXSL  = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
						+ "<xsl:stylesheet version=\"1.0\""
						+ "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
						+"<xsl:output method=\"xml\" version=\"1.0\" encoding=\"UTF-8\" indent=\"yes\"/>"
						+"<xsl:template match=\"/\">"
						+"<xsl:element name=\"root\">"
						+"<xsl:apply-templates select=\"catalog/cd/artist\"/>"
						+"<xsl:apply-templates select=\"catalog/cd/country\"/>"
						+"</xsl:element>"
						+"</xsl:template>"
						+"<xsl:template match=\"catalog/cd/artist\">"
						+"<xsl:element name=\"singer\">"
						+"<xsl:value-of select=\".\"/>"
						+"</xsl:element>"
						+"<xsl:element name=\"place\" >"
						+" <xsl:value-of select=\".\"/>"
						+" </xsl:element>"
						+"</xsl:template>"
						+"</xsl:stylesheet>";
				
				TransformerFactory factory = TransformerFactory.newInstance();
				StreamSource xslStream = new StreamSource(inputXSL);
		
		        Transformer transformer = null;
				try {
					transformer = factory.newTransformer(xslStream);
				} catch (TransformerConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
		      
				StreamSource in = new StreamSource(inputXML);
		
			StreamResult out = new StreamResult(outputXML);
		
		        try {
					transformer.transform(in, out);
				} catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		        System.out.println("The generated xml file is:" + outputXML);
  }*/

